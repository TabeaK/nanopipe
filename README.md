# NanoPipe
NanoPipe is a tool to analyze reads generated by the MinION nanopore sequencer. 
This is the command line version of the tool. 
The web version featuring a graphical user interface can be found [here](http://www.bioinformatics.uni-muenster.de/tools/NanoPipe/generate/).

If you have any enquiries, please contact *bioinfo (at) uni-muenster (dot) de*

Requirements
============

To use the Pipeline, you need to have the following software installed:

-   Python 2.7, from <https://www.python.org/downloads/>
-   LAST, at least version 587, from <http://last.cbrc.jp/>; 
-   Muscle, from <http://www.drive5.com/muscle/>
-   poretools, from <http://poretools.readthedocs.org/en/latest/>
-   R, from <https://cran.r-project.org/>

Furthermore, the following Python packages are needed:

-   rpy2
-   h5py
-   argh
-   PyYAML
-   Cython

Python packages can be installed using `easy_install`, or `pip`.

This software was tested on Max OS X (10.10.2).

Installation Instructions
=========================
To install NanoPipe, you first need to download it, and then use Python for installation. Detailed instructions are given below.

## download latest version of NanoPipe
You can install NanoPipe either from the public git repository, or by downloading the latest version from the NanoPipe website.

### clone from the git repository (recommended)
To clone the latest version of the software, use this command:

	git clone https://TabeaK@bitbucket.org/TabeaK/nanopipe.git
	
### download from website
Download the latest version of `nanopipe_v*.tar.gz` from [here](http://www.bioinformatics.uni-muenster.de/tools/NanoPipe/download-cline/latest/) and extract it. 

If the archive file was downloaded into the `Downloads/` folder, it can be extracted using this command:

    cd ~/Downloads/
    tar xzfv nanopipe_v*.tar.gz

## Install NanoPipe
When you downloaded the latest version of NanoPipe,
change into the directory `nanopipe_v*/` and run this command to install the software:

    python setup.py install

This will install the scripts into the standard location for
third-party Python modules (more information on the locations is found
[here](https://docs.python.org/2/install/index.html#how-installation-works)).

If you, for example, have downloaded the file to your `Downloads/`
directory, you can run these commands to install the NanoPipe scripts

    cd ~/Downloads/nanopipe_v*/
    python setup.py install

## Install NanoPipe at alternative location
To install NanoPipe at another location, use the command

    python setup.py install --user

You might need to set the python path with this command,

    setenv PYTHONPATH $HOME/.local/lib/python2.7

where you should use the path to your python.

Usage Example
=============

To use the Script with the example data provided, run these commands

    cd ~/Downloads/nanopipe_v*/NanoPipe/example-data/
    mkdir test_run
    cd test_run/
    nanopipe -q ../plasmodium_falciparum.fq -a plasmodium

To see a help message, run

    $ nanopipe -h
    usage: nanopipe [-h] [-v] -q QUERY_FILE -a {dengue,plasmodium,provide}
                    [-t TARGET_FILE] [-f] [-l LAST_PARAMETER [LAST_PARAMETER ...]]
                    [-V] [-d]

    Pipeline to perform analysis on Nanopore reads.

    optional arguments:
      -h, --help            show this help message and exit
      -v, --version         show program's version number and exit
      -t TARGET_FILE, --target_file TARGET_FILE
                            File with target sequences.
      -f, --use_all_reads   Use all reads from the query data (all reads, not just
                            2D high quality).
      -l LAST_PARAMETER [LAST_PARAMETER ...], --last_parameter LAST_PARAMETER [LAST_PARAMETER ...]
                            Specify last parameters to be used, for example
                            -l='-q8 -a13' (equal sign and quotes are needed!). It
                            is possible, to specify the number of parallel threads
                            to be used by last with the command -l='-PX', where X
                            is the thread number.
      -V, --verbose         Use verbose mode.
      -d, --debug           Use debug mode. Will print even more verbose log file.

    Required arguments:
      -q QUERY_FILE, --query_file QUERY_FILE
                            Path to file or directory with query data (in fastq or
                            fast5 format)
      -a {dengue,plasmodium,provide}, --analysis_type {dengue,plasmodium,provide}
                            Which analysis task to perform. Allowed types:
                            ['dengue', 'plasmodium', 'provide']

Output
======

The script creates several output files, *e.g.*

-   consensus.fasta with the consensus sequences, based on 'majority
    rule'
-   reads_per_gene.tsv with the number of reads assigned to each target
    gene
-   polymorphisms.tsv with the polymorphisms found in the target genes

