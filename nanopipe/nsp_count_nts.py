#!/usr/bin/env python

"""
Author: Tabea Kischka

Takes a tabular output from the last alignment software as an input file and creates a tabular file with the number of
occurrences of all nucleotides in the sequences. The tabular version of the last output is generated by the script
maf2tabular.py

The input file must contain these columns and must be tab-separated:

 1: qseqid (query sequence ID)
 2: qlen (entire query sequence length)
 3: sseqid (sbjct sequence ID)
 4: slen (entire sbjct sequence length)
 5: qstart (start position of alignment in query)
 6: qalnlen (length of alignment in query, does not include number of gaps)
 7: sstart (start position of alignment in sbjct)
 8: salnlen (length of alignment in sbjct, does not include number of gaps)
 9: qstrand (the query strand)
10: sstrand (the sbjct strand)
11: qseq (the query sequence from the alignment, includes gaps)
12: sseq (the sbjct sequence from the alignment, includes gaps)
13: mismap (the mismap value, from the 'a' line)
14: bitscore (the score, from the 'a' line)
15: length (the length of the alignment, includes gaps)
16: pident (the percent identity between the two sequences)
17: mismatch (the number of mismatches between the two sequences)
18: gapopen (currently always nA)

The output table contains the columns:
'sseqid',
'seqpos',
'A',
'C',
'G',
'T',
'gap',
'match',
'mismatch',
'ref',
'ref_comp',
'chrom',
'chrom_pos'

"""

import csv
import sys

which_to_convert_to_int = ['qlen',
                           'slen',
                           'qstart',
                           'sstart']
which_to_convert_to_float = ['pident',
                             'evalue',
                             'bitscore']
tabular_fieldnames = ['qseqid',
                      'qlen',
                      'sseqid',
                      'slen',
                      'qstart',
                      'qalnlen',
                      'sstart',
                      'salnlen',
                      'qstrand',
                      'sstrand',
                      'qseq',
                      'sseq',
                      'evalue',
                      'bitscore',
                      'length',
                      'pident',
                      'mismatch',
                      'gapopen']

output_header = [
    'sseqid',
    'seqpos',
    'A',
    'C',
    'G',
    'T',
    'gap',
    'match',
    'mismatch',
    'ref',
    'ref_comp',
    'chrom',
    'chrom_pos']

"""
FUNCTIONS
"""


def convert_str_to_int_or_float_in_dict(dict_row):
    """
    Takes dictionary row from DictReader, converts
    all specified fields into integers and returns
    this new dictionary row.

    :param dict_row: contains the content of one
    row of the input file in dictionary format
    :return: a modified version of the dictionary,
    in which the numerical values are converted to
    either int or float
    """
    for col_name in which_to_convert_to_int:
        try:
            dict_row[col_name] = int(dict_row[col_name])
        except ValueError:
            pass
    for col_name in which_to_convert_to_float:
        try:
            dict_row[col_name] = float(dict_row[col_name])
        except ValueError:
            pass
    return dict_row


def get_empty_counting_list_dict(seqlen):
    """
    Returns a list of empty dictionaries to count
    the number of occurrences of nucleotides.
    The list looks something like this:
    [{'A':0,'C':0,'T':0,'G':0, '-':0, 'match':0, 'mismatch':0, 'ref':'nA'},...]
    """
    nt_counts = []
    if not isinstance(seqlen, int):
        print("ERROR: Please make sure that tabular alignment file " +
              "does not contain a output_header line.")
        sys.exit(1)
    for i in xrange(seqlen):
        nt_counts.append({'A': 0,
                          'C': 0,
                          'T': 0,
                          'G': 0,
                          '-': 0,
                          'match': 0,
                          'mismatch': 0,
                          'ref': 'nA'
        })
    return nt_counts


def get_chrom(gene_name):
    """
    Returns the name of the chromosome on which
    this gene is located. It is extracted from
    the chromosome name.
    e.g.: >Pf3D7_01_v3:264641-267470:+:PfATPase6_2
    will return 'Pf3D7_01_v3'
    """
    try:
        gene_name_split = gene_name.split(':')
    except IndexError:
        return 'nA'
    if len(gene_name_split) == 1:
        return 'nA'
    else:
        return gene_name_split[0]


def get_strand(gene_name):
    """
    Returns the strand a gene is located on.
    e.g.: >Pf3D7_01_v3:264641-267470:+:PfATPase6_2
    will return '+'
    """
    gene_name_split = gene_name.split(':')
    try:
        gene_strand = gene_name_split[2]
    except IndexError:
        return 'nA'
    if gene_strand == '+' or gene_strand == '-':
        return gene_strand
    else:
        print('Invalid strand (%s) for gene %s' % (gene_strand, gene_name))
        sys.exit(1)


def get_chrom_pos(gene_name, pos):
    """
    Returns the position of a nucleotide on the chromosome.
    e.g.: 'Pf3D7_01_v3:264641-267470:+:PfATPase6_2', 10 will return 264651

    Only works for the task plasmodium polymorphisms and with the
    specific target genes used by this analysis, since the function
    depends on the specific formatting of names, with the chromosome
    position of the gene right after the gene name.

    :param gene_name: The string containing the gene's name
    :param pos: The nucleotide's position in the gene's sequence
    :return: either 'nA', if the chromosome position of the gene is unknown,
             or the position of this nucleotide on the chromosome
    """
    gene_name_split = gene_name.split(':')
    if len(gene_name_split) < 2:
        return 'nA'
    elif len(gene_name_split[1].split('-')) != 2:
        return 'nA'
    try:
        gene_start = int(gene_name_split[1].split('-')[0])
        gene_end = int(gene_name_split[1].split('-')[1])
        gene_strand = gene_name_split[2]
        if gene_strand == '+':
            return gene_start + pos
        elif gene_strand == '-':
            return gene_end - pos
        else:
            print('Invalid strand (%s) for gene %s' % (gene_strand, gene_name))
            sys.exit(1)
    except ValueError:
        return 'nA'


def get_complement(nt):
    d = {'A': 'T',
         'C': 'G',
         'G': 'C',
         'T': 'A',
         'nA': 'nA'}
    return d[nt]


def get_ref(nt_count, gene_name):
    """
    Get the complementary nucleotide, if the sequence is
    on the minus strand.
    :param nt_count: the nt_count dictionary
    :param gene_name: the name of the gene (to extract strand from)
    :return: single character string
    """
    gene_strand = get_strand(gene_name)
    if gene_strand == '+':
        return nt_count['ref']
    else:
        return get_complement(nt_count['ref'])


def get_all_nt_counts_by_sbjct():
    """
    Will create a big, nested dictionary. From outside to inside this
    dictionary is structured like this:
    The subject ID used as a key, and it contains a list of "counting"
    dictionaries. The number of these dictionaries is equivalent to
    the length of the subject sequence, so that for each position in
    the subject sequence there is a "counting" dictionary.

    :return: nt_counts
    """
    # open input file
    tab_aln_handle = open(sys.argv[1], 'r')
    tab_aln_csv = csv.DictReader(tab_aln_handle,
                                 dialect='excel-tab',
                                 fieldnames=tabular_fieldnames)
    nt_counts = {}

    # goes through each line in the alignment file (i.e. each alignment of sbjct
    # and query) once at a time
    for k, aln_line in enumerate(tab_aln_csv):

        # convert strings to integers/floats
        aln_line = convert_str_to_int_or_float_in_dict(aln_line)

        # check, if this is the first time this sbjct sequence
        # is occurring, if so, create a list of empty dictionaries to store the data in later on
        if not aln_line['sseqid'] in nt_counts:
            nt_counts[aln_line['sseqid']] = get_empty_counting_list_dict(aln_line['slen'])

        # #
        # count the nucleotides
        # #

        # pos_in_aln marks the position in the alignment (including gaps), not the complete sequence!
        # pos_in_fullseq marks the position in the complete sequence (excluding gaps)!
        pos_in_aln = 0
        pos_in_fullseq = aln_line['sstart']

        # iterate over every position in the alignment
        for pos, each_nt_position in enumerate(aln_line['sseq']):

            sbjct_nt = aln_line['sseq'][pos_in_aln]
            query_nt = aln_line['qseq'][pos_in_aln]

            # when there is a gap in the sbjct sequence, ignore this
            # position and move on to the next position in the alignment,
            # but do not move to the next position in the sbjct seq!
            if sbjct_nt == '-':
                pass

            # no gap in sbjct
            else:
                # if there is no gap in the sbjct sequence store the nucleotides
                # in the dictionary
                if nt_counts[aln_line['sseqid']][pos_in_fullseq]['ref'] != 'nA':
                    # if this is not the first time, this position in the reference sequence
                    # is visited, check if the nucleotide is identical (which it should be!)
                    if nt_counts[aln_line['sseqid']][pos_in_fullseq]['ref'] != sbjct_nt:
                        print("ERROR: Problem in parsing alignment. The same subject sequence "
                              "seems to differ between some alignments: %s. %s != %s \n" % (
                                  aln_line['sseqid'], nt_counts[aln_line['sseqid']][pos_in_fullseq]['ref'], sbjct_nt))
                        print("%s %s %s %s %s " % (
                            pos_in_aln, pos_in_fullseq, sbjct_nt, nt_counts[aln_line['sseqid']][pos_in_fullseq]['ref'],
                            query_nt))
                        sys.exit(1)
                # store the current sbjct nucleotide as the reference nucleotide
                nt_counts[aln_line['sseqid']][pos_in_fullseq]['ref'] = sbjct_nt

                # increase the nucleotide count by 1, for the respective nucleotide in the query sequence
                nt_counts[aln_line['sseqid']][pos_in_fullseq][query_nt] += 1

                # if this query nucleotide is identical to the reference one, increase the match count,
                # if not, increase the mismatch count
                if query_nt == sbjct_nt:
                    nt_counts[aln_line['sseqid']][pos_in_fullseq]['match'] += 1
                else:
                    nt_counts[aln_line['sseqid']][pos_in_fullseq]['mismatch'] += 1

                pos_in_fullseq += 1
            pos_in_aln += 1
    tab_aln_handle.close()
    return nt_counts


def write_table_from_nucleotide_counts(nt_counts, output_handle):
    """
    Generates a table from the list of dictionaries
    containing the nucleotide counts. The table contains
    all data for all subject sequences in the following
    format:
        1: Name of (target) gene
        2: Position in (target) gene
        3: Number of A's in aligned reads at this position
        4: "" C ""
        5: "" G ""
        6: "" T ""
        7: "" gaps ""
        8: Number of times the nucleotide in a read is the same as in the (target) gene
        9: "" not the same ""
       10: The nucleotide that is found at this position in the (target) gene.
       11: "" on the reverse strand
       12: The chromosome the target gene is located on
       13: The position on the chromosome the target gene is located on
       14:

    Note for last column: This sequence is based on the alignments, hence,
    if no read aligns to a position in the (target) gene, then the nucleotide
    at this position is also not known.
    """
    output_handle.write('\t'.join(output_header) + '\n')
    for sseqid in nt_counts.keys():
        # now get the list of dictionaries
        sseq_nt_counts = nt_counts[sseqid]
        for seq_pos, nt_count in enumerate(sseq_nt_counts):
            # nt_count is the dictionary for a given position
            # seq_pos in the alignment
            output_handle.write('\t'.join([
                sseqid,
                str(seq_pos),
                str(nt_count['A']),
                str(nt_count['C']),
                str(nt_count['G']),
                str(nt_count['T']),
                str(nt_count['-']),
                str(nt_count['match']),
                str(nt_count['mismatch']),
                str(nt_count['ref']),
                get_ref(nt_count, sseqid),
                get_chrom(sseqid),
                str(get_chrom_pos(sseqid, seq_pos))
            ]) + '\n')


def main():
    output_handle = open(sys.argv[2], 'w')
    all_nt_counts_by_sbjct = get_all_nt_counts_by_sbjct()
    write_table_from_nucleotide_counts(all_nt_counts_by_sbjct, output_handle)


if __name__ == "__main__":
    main()
