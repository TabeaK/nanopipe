#!/usr/bin/env python

"""
Author: Tabea Kischka

Takes a maf alignment and converts it into a tabular format.

Arguments are:
1) Input MAF file
2) Name of output file in tab format

Use like this:
maf2tabular.py aln.maf aln.tab

The columns in the tabular format are as follows:

 1: qseqid (query sequence ID)
 2: qlen (entire query sequence length)
 3: sseqid (sbjct sequence ID)
 4: slen (entire sbjct sequence length)
 5: qstart (start position of alignment in query)
 6: qalnlen (length of alignment in query, does not include number of gaps)
 7: sstart (start position of alignment in sbjct)
 8: salnlen (length of alignment in sbjct, does not include number of gaps)
 9: qstrand (the query strand)
10: sstrand (the sbjct strand)
11: qseq (the query sequence from the alignment, includes gaps)
12: sseq (the sbjct sequence from the alignment, includes gaps)
13: mismap (the mismap value, from the 'a' line)
14: bitscore (the score, from the 'a' line)
15: length (the length of the alignment, includes gaps)
16: pident (the percent identity between the two sequences)
17: mismatch (the number of mismatches between the two sequences)
18: gapopen (currently always nA)
"""

import os
import sys


def get_pident_mismatch_gapopen(sseq, qseq):
    mismatch_nr = 0
    match_nr = 0
    for pos, sletter in enumerate(sseq):
        qletter = qseq[pos]
        if sletter == qletter:
            match_nr += 1
        else:
            mismatch_nr += 1
    pident = (match_nr * 100.0) / len(qseq)
    return str(pident), str(mismatch_nr), 'nA'


def main():
    try:
        maf_file = sys.argv[1]
        output_filename = sys.argv[2]
    except IndexError:
        print "Use like this:\npython maf2tabular.py [file with alignment in maf format] [output filename]"
        sys.exit(1)
    maf_file1line = '%s.1line' % maf_file
    # the following grep/perl command skips all comment lines and concatenates all the lines of each alignment
    # into one single line
    cmd = "grep -v '^#' %s | perl -pe 's/\n/\t/ if /\S/' > %s" % (maf_file, maf_file1line)
    os.system(cmd)
    output_fh = open(output_filename, 'w')
    with open(maf_file1line, 'r') as fh:
        for line in fh.readlines():
            line1, line2, line3, line4, line5 = line.split('\t')[0:5]

            # example for line1: a score=1181 mismap=1e-10
            score  = line1.split()[1].split('=')[1]
            mismap = line1.split()[2].split('=')[1]

            # example for line2/3: s file111_strand.fast5 72 679 - 844 TATAGCTTTGAATTT...
            sbjctname, sbjctstart, sbjctalnlen, sbjctstrand, sbjcttotlen, sbjctseq = line2.split()[1:]
            queryname, querystart, queryalnlen, querystrand, querytotlen, queryseq = line3.split()[1:]

            pident, mismatch, gapopen = get_pident_mismatch_gapopen(sbjctseq, queryseq)

            output_fh.write('\t'.join([queryname,
                                       querytotlen,
                                       sbjctname,
                                       sbjcttotlen,
                                       querystart,
                                       sbjctalnlen,
                                       sbjctstart,
                                       queryalnlen,
                                       querystrand,
                                       sbjctstrand,
                                       queryseq,
                                       sbjctseq,
                                       mismap,
                                       score,
                                       str(len(queryseq)),
                                       pident,
                                       mismatch,
                                       gapopen]) + '\n')
    output_fh.close()
    os.remove(maf_file1line)


if __name__ == "__main__":
    main()