#!/opt/local/bin/R
# Part of the NanoPipe pipeline.
# This script creates the consensus sequence from the nucleotide counts file
# it also creates logo barplots


#############
# FUNCTIONS #
#############

# consensus related functions
get_consensus_in_row <- function(x){
    # Returns the consensus nucleotide for each row/column in the alignment
    # by finding the most common nucleotide(s)
    # If not a single nucleotide is the most common one, use IUPAC notation
    if(sum(x) < min_number_of_reads){
        return("N")
    }
    max_value = max(x)
    col_with_max_value = which(x %in% max_value)
    how_often_is_max_value_found = length(col_with_max_value)
    if (how_often_is_max_value_found == 2){
        if(all(col_with_max_value == c(1,2))){return("M")} # A & C -> M
        if(all(col_with_max_value == c(1,3))){return("R")} # A & G -> R
        if(all(col_with_max_value == c(1,4))){return("W")} # A & T -> W
        if(all(col_with_max_value == c(2,3))){return("S")} # C & G -> S
        if(all(col_with_max_value == c(2,4))){return("Y")} # C & T -> Y
        if(all(col_with_max_value == c(3,4))){return("K")} # G & T -> K
    }
    else if (how_often_is_max_value_found == 3){
        if(all(col_with_max_value == c(1,2,3))){return("V")} # A & C & G -> V
        if(all(col_with_max_value == c(1,2,4))){return("H")} # A & C & T -> H
        if(all(col_with_max_value == c(1,3,4))){return("D")} # A & G & T -> D
        if(all(col_with_max_value == c(2,3,4))){return("B")} # C & G & T -> B        
    }
    else if (how_often_is_max_value_found == 4){
        if (max_value == 0){
            return("N")
        }else{
            return("N")
        }
    }    
    else{
        return(names(which.max(x)))
    }
}

get_consensus_nts <- function(d){
    # Returns the entire consensus sequence for all rows
    consensus <- apply(d[,3:6], 1, get_consensus_in_row)
    return(as.matrix(consensus))
} 

get_consensus_string <- function(d){
    # introduce "N" sign for unknown ("nA") nucleotide in consensus string
    if (("N" %in% levels(d$consensus))==FALSE){
        consensus = factor(d$consensus, levels=c(levels(d$consensus), "N"))
    }
    else{
        consensus = d$consensus
    }
    # replace "nAs" with "N"
    consensus = replace(consensus, consensus=='nA', 'N')
    # get a string of the sequence
    consensus = paste(consensus, collapse="")
    return(consensus)
}

get_ref_string <- function(d){
    # returns the reference string, replacing all "nA" by N
    ref = paste(d$ref, collapse="")
    ref = gsub('nA', 'N', ref)
    return(ref)
}

get_nr_consensus_ref_identical <- function(d){
    # Returns the number of times the consensus and
    # the reference sequences are identical; ignores
    # sequence positions which are gap in the reference
    # sequence, i.e. are covered by none of the aligned
    # reads
    dn <- subset(d, d$ref!='nA')
    nr_identical <- dim(subset(dn, dn$consensus == dn$ref))[1]
}

get_nr_consensus_ref_nonidentical <- function(d){
    # Returns the number of times the consensus and
    # the reference sequences are NOT identical; ignores
    # sequence positions which are gap in the reference
    # sequence, i.e. are covered by none of the aligned
    # reads
    dn <- subset(d, d$ref!='nA')
    nr_identical <- dim(subset(dn, dn$consensus != dn$ref))[1]
}

get_nt_var_in_row <- function(x){
    return(max(x)/sum(x))
}

get_perc_difference <- function(d){
    # Returns the percentage difference between consensus
    # and reference sequence
    nr_consensus_is_ref = get_nr_consensus_ref_identical(d)
    nr_consensus_isnt_ref = get_nr_consensus_ref_nonidentical(d)
    perc_consensus_isnt_ref = nr_consensus_isnt_ref/(nr_consensus_is_ref + 
                                                         nr_consensus_isnt_ref)
    perc_consensus_isnt_ref = round(perc_consensus_isnt_ref*100, 2)
    perc_consensus_isnt_ref = paste(perc_consensus_isnt_ref, '%')
    return(perc_consensus_isnt_ref)
}

get_polymorphic_sites <- function(d, polymorphic_cutoff){
    # returns a subset of polymorphic sites, based on the
    # field nt_var, which represents the variation
    return(subset(d, d$nt_var < polymorphic_cutoff))
}

write_seq_to_file <- function(seq, name, filename='seq.fasta'){
    # writes a given fasta sequence to a file
    # is used to write the consensus sequences
    write(paste(
        ">", name, "\n", seq, sep=""), 
        file=filename,
        append=TRUE)
}

# plot related functions
get_color_vector_for_seq <- function(sequ){
    # Returns the colors to be used in the logo barplot
    color_list = list(A="#CC333F",
                      B="#87BF77",
                      C="#8fb72e",
                      D="#A17F66",
                      G="#EDC951",
                      H="#5E8D7B",
                      K="#81B77E",
                      M="#C28349",
                      N="grey",
                      R="#DE7B48",
                      S="#CDCD5B",
                      T="#00A0B0",
                      V="#CD7D51",
                      W="#7C5B6F",
                      Y="#34AC93",
                      nA="grey")
    color_vector = c()
    for(s in sequ){
        color_vector = c(color_vector, color_list[[ s[1] ]])
    }
    return(color_vector)
}

plot_logo_barplot <- function(d, filename=args[1]){
    # creates a logo barplot
    opar <- par(lwd = 0.001)
    # plot main barplot
    barplot(t(d[,3:7]), 
            col=nt_cols, 
            border=0,
            main=paste(d[1,1], 
                       'of length', 
                       max(d$seqpos), 
                       'with percentage difference of ',
                       get_perc_difference(d),
                       ' between reference and consensus; generated from file ', 
                       filename,
                       ' on:', 
                       Sys.time()
            ),
            xlab="Reference sequence and aligned reads",
            ylab="Nucleotides in nanopore read TEST",
            las=2,
            xaxt='n',
            xaxs="i",
            ann=FALSE)
    legend("topleft", legend=colnames(d)[3:7], col=nt_cols, pch=15)
    
    # add bars representing original sequence
    plot_add_original_seq(d)

    # add consensus
    par(xpd=NA, new=T);
    barplot(rep(-0.7, dim(d)[1]), border=0, 
            col=get_color_vector_for_seq(d$consensus), 
            xpd=T, axes=F, 
            xaxs="i",
            ylim=c(-0.05,35))
    
    # add sequence position indicators
    # get the positions of the bars on the x axis
    d.xpositions <- barplot(t(d[,3:7]),plot=F)
    par(xpd=NA, new=T)
    d.seqpos_interval = 10
    
    if (d$chrom_pos[1] == 'nA'){
        d.chrom_pos = 0        
    }
    else{
        d.chrom_pos = d$chrom_pos[1]        
    }
    
    d.seqchromstart = d.chrom_pos + d.seqpos_interval
    offset = d.seqpos_interval - (d.seqchromstart %% 10)
    d.seqchromstartOffset = d.seqchromstart + offset
    d.whichseqposLabels = seq(d.chrom_pos + d.seqpos_interval + offset, d.chrom_pos + max(d$seqpos), d.seqpos_interval)
    d.whichseqpos = seq(d.chrom_pos + d.seqpos_interval + offset, d.chrom_pos + max(d$seqpos) + offset, d.seqpos_interval)
    
    
    text(d.xpositions[d.whichseqpos - offset - d.chrom_pos] + offset, -3.5, labels=d.whichseqposLabels, srt=90, cex=0.7)
    text(-2.3, c(-0.3, -1.1), labels=c('con', 'ref'), cex=0.75, adj=0, col='#333333')
    #text(d.xpositions[d.whichseqpos], -1.6, labels=".")
    segments(y0=-0.7, y1=-0.7, x0=0, x1=max(d$seqpos), lwd=1, lty=1, col='#333333')
}

plot_add_original_seq <- function(d){
    # add bars representing original sequence/reference to the logo barplot
    par(xpd=NA, new=T);    
    barplot(
        rep(-1.4, 
            dim(d)[1]), 
        border=0, 
        col=get_color_vector_for_seq(d$ref), 
        xpd=T, 
        xaxs="i",
        axes=F, 
        ylim=c(-0.05,35)
    )    
}

plot_polymorphic <- function(d, polymorphic_cutoff){
    # make a logo barplot for only the polymorphic sites
    d = get_polymorphic_sites(d, polymorphic_cutoff)
    if(dim(d)[1]==0){return()}
    opar = par(lwd = 0.001)
    # plot main barplot
    barplot(t(d[,3:7]), 
            col=nt_cols, 
            border=0,
            main=paste('Polymorphic sites for ',
                       d[1,1]
            ),
            xlab="Reference Sequence and aligned Reads",
            ylab="Nucleotides in nanopore read",
            las=2,ann=FALSE)
    legend("topleft", legend=colnames(d)[3:7], col=nt_cols, pch=15)
    plot_add_original_seq(d)
}

# polymorphism table related functions
write_polymorphism_table <- function(d, filename='polymorphisms.tsv'){
    # write the polymorphic sites into a table
    d = subset(d, d$cons_mismatch==TRUE)
    d = subset(d, select=c(sseqid, seqpos, 
                           ref, consensus, 
                           A, C, G, T, 
                           gap, 
                           sum, nt_var, 
                           chrom, chrom_pos))
    d = subset(d, d$sum>=min_number_of_reads)
    # make the sequence position 1-, and not 0-based!
    d$seqpos <- d$seqpos+1
    d$nt_var <- round(d$nt_var, 3)
    write.table(d, 
                file=filename, 
                sep='\t',
                quote=FALSE,
                row.names=F)
}


########
# MAIN #
########

nt_cols=c("#CC333F", "#8fb72e", "#EDC951", "#00A0B0", "grey")
polymorphic_cutoff  = 1 #0.5
min_number_of_reads = 10

args <- commandArgs(trailingOnly = TRUE)
d    <- read.delim(args[1], stringsAsFactors=FALSE)
sum  <- apply(d[,3:6],1,sum)
d    <- cbind(d, sum)
consensus <- get_consensus_nts(d)
d         <- cbind(d, consensus)
levels(d$consensus) <- c(levels(d$consensus), 'nA')
nt_var <- apply(d[,3:6], 1, get_nt_var_in_row)
d      <- cbind(d, nt_var)

# get the match and mismatch column, based on the identity between the
# consensus and the reference nucleotide
cons_match    <- factor(d$ref, levels=levels(d$consensus))==d$consensus
cons_mismatch <- factor(d$ref, levels=levels(d$consensus))!=d$consensus
d    <- cbind(d, cons_match, cons_mismatch)


## Generate Logo Barplots for all Sequences
i=0
for (sbjct in sort(unique(d$sseqid))){
    # make logo plot
    d.sseqid <- subset(d, d$sseqid == sbjct)
	pdf_filename = paste('plot', sprintf("%03d", i), '.pdf', sep='')
	pdf(pdf_filename,  
    	width=max(8, max(d.sseqid$seqpos)/10), 
    	height=max(6, max(d.sseqid$sum)/10))
    plot_logo_barplot(d.sseqid)
    dev.off()
#     pdf_filename = paste('plot_polymorph', i, '.pdf', sep='')
#     pdf(pdf_filename,  
#         width=max(8, max(d.sseqid$seqpos)/100), 
#         height=max(6, max(d.sseqid$sum)/10))    
#     plot_polymorphic(d.sseqid, polymorphic_cutoff)
#     dev.off()
    i=i+1
}

## Write the consensus sequences to a single fasta file
for (sbjct in sort(unique(d$sseqid))){
    write_seq_to_file(get_consensus_string(subset(d, d$sseqid==sbjct)), 
                      paste(sbjct, 'consensus', sep='_'), 
                      filename='consensus.fasta')
}

## Write the polymorphisms table into a single csv file
write_polymorphism_table(d)

## Write the entire nt counts table with the consensus into a file
write.table(d, 
            file='nt_counts_plus_consensus.tsv', 
            sep='\t',
            quote=FALSE, 
            row.names=F)
