#!/usr/bin/env python
# coding=utf-8

"""
Author: Tabea Kischka


Script to handle nanopore analysis.

It receives 4 arguments:
    * name of input query file in fastq or fast5/zip
    * name of input target file in fasta format
    * which kind of input data is given: either Dengue or Plasmodium
"""

import os
import sys
import shutil
import subprocess
import argparse
import zipfile
import tarfile
import csv
import logging
from _version import __version__


# script and matrices
last_Pf_sub_matrix = os.path.join(os.path.dirname(__file__), 'data', 'matrix_Pf.txt')
maf2tabular_script = os.path.join(os.path.dirname(__file__), 'nsp_maf2tabular.py')
nt_count_script = os.path.join(os.path.dirname(__file__), 'nsp_count_nts.py')
generate_results_script = os.path.join(os.path.dirname(__file__), 'nsp_generate_results.py')
plot_nt_count_rscript = os.path.join(os.path.dirname(__file__), 'R_scripts', 'plot_nt_counts.R')
plot_len_script = os.path.join(os.path.dirname(__file__), 'R_scripts', 'plot_lengths.R')

# lastdb files
lastdb_files = {'plasmodium': os.path.join(os.path.dirname(__file__), 'data', 'myDB_plasmodium_falciparum'),
                'dengue': os.path.join(os.path.dirname(__file__), 'data', 'myDB_Dengue_virus')}
# target files (will be used to perform muscle alignment displayed in results)
target_seq_files = {'plasmodium': os.path.join(os.path.dirname(__file__), 'data',
                                               'target_seq_plasmodium_falciparum.fasta'),
                    'dengue': os.path.join(os.path.dirname(__file__), 'data',
                                           'target_seq_Dengue_Virus_4_serotypes.fasta')}
# SNP info file
snp_info_file = os.path.join(os.path.dirname(__file__), 'data',
                             'targets_polymorphisms_MalariaGen_v3_and_drugresistance.csv')

snp_output_header = ['Gene name',
                     'Position in gene',
                     'Reference',
                     'Consensus',
                     'A',
                     'C',
                     'G',
                     'T',
                     'gap',
                     'Total read nr',
                     'Variation',
                     'Chromosome',
                     'Position on chromosome']
# snp_info_fields is needed to give the order in which the columsn should appear
snp_info_fields = ["Chromosome", "Position", "SnpName", "AminoAcid", "NRAF_WAF", "NRAF_CAF", "NRAF_EAF", "NRAF_SAS",
                   "NRAF_WSEA", "NRAF_ESEA", "NRAF_PNG", "NRAF_SAM", "MAF_WAF", "MAF_CAF", "MAF_EAF", "MAF_SAS",
                   "MAF_WSEA",
                   "MAF_ESEA", "MAF_PNG", "MAF_SAM", "Type", "ref", "alt", "ancestral", "pvtAllele", "pvtPop",
                   "Publication", "DrugResistanceMutation", ]

# allowed task types
allowed_task_types = ['dengue', 'plasmodium', 'provide']
# known file extensions
known_file_ext = ['.fastq', '.fast5', '.fq', '.f5']
# last parameters that are always used
standard_last_parameters = ['-Q1', '-m100', '-j4']
last_parameter_by_task = dict(plasmodium=['-p',
                                          last_Pf_sub_matrix,
                                          '-a17',
                                          '-b2',
                                          '-e180'],
                              dengue=['-q12',
                                      '-a15',
                                      '-b3',
                                      '-e180'],
                              provide=['-q12',
                                       '-a15',
                                       '-b3',
                                       '-e180'])

"""
FUNCTIONS
"""


def get_time():
    from time import localtime, strftime
    return strftime("%Y-%m-%d %H:%M:%S", localtime())


def replace_file_extension(filename, new_ext):
    """
    Replaces the file extension of a given file.
    """
    if not new_ext.startswith('.'):
        new_ext = '.%s' % new_ext
    return os.path.splitext(filename)[0] + new_ext


def allowed_last_parameter(string):
    """
    checks, whether the user provided valid LAST parameters
    """
    known_param = ["-r", "-q", "-p", "-a", "-b", "-A", "-B", "-c", "-F", "-x", "-y", "-z", "-d", "-e", "-s", "-T", "-m",
                   "-l", "-L", "-n", "-C", "-k", "-i", "-R", "-u", "-w", "-G", "-t", "-g"]
    for param in string.split():
        if param[0:2] not in known_param:
            msg = "The parameter '%s' that you provided is not a valid last parameter. " \
                  "The entire parameter string given by you was: '%s'." % (param, string)
            raise argparse.ArgumentTypeError(msg)
            log.warning(msg)
    return string


def get_input_arguments():
    """
    This function sets the allowed arguments and parses them.
    It returns the user-specified values for the input query
    and target files and the task to perform.
    """
    parser = argparse.ArgumentParser(
        description="Pipeline to perform analysis on Nanopore reads.", version=__version__)
    requirednamed = parser.add_argument_group('Required arguments')
    requirednamed.add_argument(
        "-q",
        "--query_file",
        help="Path to file or directory with query data (in fastq or fast5 format)",
        required=True)
    requirednamed.add_argument(
        "-a",
        "--analysis_type",
        help="Which analysis task to perform. Allowed types: %s" % allowed_task_types,
        choices=allowed_task_types,
        required=True)
    parser.add_argument(
        "-t",
        "--target_file",
        help="File with target sequences.",
        required=False)
    parser.add_argument(
        "-m",
        "--substitution_matrix",
        help="File with substitution matrix.",
        required=False)
    parser.add_argument(
        "-f",
        "--use_all_reads",
        help="Use all reads from the query data (all reads, not just 2D high quality).",
        required=False,
        default=False,
        action='store_true')
    parser.add_argument(
        "-l",
        "--last_parameter",
        help="Specify last parameters to be used, for example -l='-q8 -a13' (equal sign and quotes are needed!).\n"
             "It is possible, to specify the number of parallel threads to be used by last with the command -l='-PX', "
             "where X is the thread number.",
        required=False,
        nargs='+',
        action='store',
        type=allowed_last_parameter,
        default=[])
    parser.add_argument(
        "-V",
        "--verbose",
        help="Use verbose mode.",
        required=False,
        action='store_true',
        default=False)
    parser.add_argument(
        "-d",
        "--debug",
        help="Use debug mode. Will print even more verbose log file.",
        required=False,
        action='store_true',
        default=False)
    args = parser.parse_args()
    query = args.query_file
    input_task = args.analysis_type
    use_all_reads = args.use_all_reads
    user_last_parameters = args.last_parameter
    substitution_matrix = args.substitution_matrix

    # set logging parameters
    if args.verbose:
        log.setLevel(logging.INFO)
    if args.debug:
        log_fh.setLevel(logging.DEBUG)
        log.setLevel(logging.DEBUG)

    if input_task != 'provide':
        lastdb_path = lastdb_files[input_task]
        target_file = target_seq_files[input_task]
    else:
        # Create target database
        if not args.target_file:
            log.error("When the task is not one of %s a target file needs to be provided!"
                      " Task was '%s'." % (allowed_task_types, input_task))
            sys.exit(1)
        target_file = args.target_file
        replace_spaces_in_fasta_header(target_file)
        lastdb_path = get_lastdb_database(target_file)
    return query, lastdb_path, input_task, target_file, use_all_reads, user_last_parameters, substitution_matrix


def execute_command(command, output_filename=None, save_stdout_to_file=False, use_os_system=False):
    """
    Runs a command on the command line.
    IMPORTANT: command needs to be in list format!
    """
    assert isinstance(command, list), 'Command needs to be a list, but was %s' % type(command)
    if not use_os_system:
        log.info('Will run command "%s" from working dir %s using subprocess' % (' '.join(command), os.getcwd()))
        process = subprocess.Popen(command,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE)
        stdout, stderr = process.communicate()
        if save_stdout_to_file is not False:
            fh = open(output_filename, 'w')
            fh.write(stdout)
            fh.close()
            log.debug("Output of command '%s' will be saved to '%s'." % (' '.join(command), output_filename))
        if stderr == '':
            log.info("Command '%s' ran successfully." % ' '.join(command))
        else:
            log.critical("Command '%s' produced the following error:\n%s" %
                         (' '.join(command), stderr))
            sys.exit(1)
    else:
        log.info('Will run command "%s" from working dir %s using os.system' % (' '.join(command), os.getcwd()))
        if save_stdout_to_file is not False:
            os.system('%s > %s' % (' '.join(command), output_filename))
        else:
            os.system('%s' % ' '.join(command))
        log.info("Command '%s' ran successfully." % ' '.join(command))


def replace_spaces_in_fasta_header(filename):
    """
    Replaces spaces in fasta headers with underscores.
    Will only be invoked for the task type "provide"
    """
    import os
    with open(filename, 'r') as f:
        lines = f.readlines()
    with open(filename + '.tmp', 'w') as f:
        for line in lines:
            if line.startswith('>'):
                f.write(line.replace(' ', '_'))
            else:
                f.write(line)
    os.rename(filename + '.tmp', filename)


def get_lastdb_database(input_file_target):
    """
    Creates the lastdb databases.
    """
    # make fasta file uppercase
    fasta2upper_command = "awk '{ if(/>/) print($0); else print toupper($0)}' target > target.upper;" \
                          " mv target.upper target"
    os.system(fasta2upper_command)
    lastdb_path = 'myDB'
    lastdb_cmd = ['lastdb', lastdb_path, input_file_target]
    execute_command(lastdb_cmd, use_os_system=True)
    return lastdb_path


def perform_last_alignment(query,
                           lastdb_path,
                           input_task,
                           output_filename,
                           lastal_path,
                           lastsplit_path,
                           substitution_matrix,
                           user_last_parameters=[],
                           use_os_system=True,
                           file_format='-Q1'):
    """
    Performs the commands lastal and last-split.
    """
    last_parameters = user_last_parameters + standard_last_parameters + last_parameter_by_task[input_task]
    last_output_filename_before_lastsplit = '%s_before_split' % output_filename

    # add the substitution matrix parameter
    if substitution_matrix:
        if os.path.isfile(substitution_matrix):
            if os.stat(substitution_matrix).st_size > 0:
                last_parameters += ['-p', substitution_matrix]
    elif input_task == 'plasmodium':
        last_parameters += ['-pATMAP']

    # perform last and last split
    last_cmd = [lastal_path] + last_parameters + [lastdb_path, query]
    execute_command(last_cmd,
                    last_output_filename_before_lastsplit,
                    save_stdout_to_file=True,
                    use_os_system=True)
    last_split_cmd = [lastsplit_path, last_output_filename_before_lastsplit]
    execute_command(last_split_cmd,
                    output_filename,
                    save_stdout_to_file=True,
                    use_os_system=True)


def perform_muscle_alignment(filename, output_filename):
    """
    Performs an alignment with muscle
    """
    muscle_command = ['muscle', '-in', filename, '-out', output_filename, '-quiet']
    execute_command(muscle_command)


def get_subdir_with_seq_files(dirname):
    """
    If an archive file is provided by the user, it might contain
    one ore more levels of subfolders. This function returns
    the path of the first folder containing fastq/fast5 files.
    """
    if not os.path.isdir(dirname):
        log.error("%s is not a directory!" % dirname)
        sys.exit(1)
    found_dir = []
    for root, dirs, files in os.walk(dirname):
        for each_file in files:
            if os.path.splitext(each_file)[1] in known_file_ext:
                if root not in found_dir:
                    found_dir.append(root)
    if len(found_dir) > 1:
        log.error('ERROR: There is more than one subdirectory with sequence' +
                  ' files! The subdirectories found to contain sequence files' +
                  'are: %s' % ', '.join(found_dir))
        sys.exit(1)
    if not found_dir:
        return False
    else:
        return found_dir[0]


def convert_fast5_to_fastq(query, use_all_reads):
    """
    Takes the name of a fast5 file or the name of a directory containing
    fast5 files and converts the data into fastq format.
    Afterwards the fast5 file provided by the user (or the folder)
    is renamed by appending '.fast5' and the new fastq file is renamed
    into the original query filename.

    IMPORTANT: Currently only the high quality 2D sequences from the fast5
    files are used!

    """
    # Remove empty fast5 files (if it is a dir)!
    if os.path.isdir(query):
        for filename in os.listdir(query):
            if not os.path.getsize(os.path.join(query, filename)) > 0:
                os.remove(os.path.join(query, filename))

    # perform poretools
    if use_all_reads == False:  # this is the default!
        poretools_command = ['poretools', 'fastq', '--type', '2D',
                             '--high-quality', query]
    elif use_all_reads == True:
        poretools_command = ['poretools', 'fastq', query]
    poretools_output_file = query + '.fastq'
    execute_command(poretools_command,
                    poretools_output_file,
                    save_stdout_to_file=True)
    # rename original input file
    os.rename(query, query + '.fast5')
    os.rename(poretools_output_file, query)
    log.info("Converted fast5 file into fastq format.")
    return query


def yield_fasta(filename):
    """
    Yields a tuple of the name
    and sequence of each entry
    Based on: https://www.biostars.org/p/710/
    """
    from itertools import groupby
    fh = open(filename)
    faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
    for header in faiter:
        # drop the ">"
        header = header.next()[1:].strip()
        # join all sequence lines to one.
        seq = "".join(s.strip() for s in faiter.next())
        yield header, seq


def check_if_last_produced_alignment(maf_filename):
    """
    Returns TRUE, when last produced any alignment, and FALSE if not.
    Empty maf alignment output files will contain only the comment
    lines which all start with '#'
    """
    filehandle = open(maf_filename, 'r')
    for line in filehandle:
        if not line.startswith('#'):
            return True
    return False


def unzip_file(file_to_unzip, target_dir):
    """
    Unzip files in file_to_unzip to target dir
    """
    log.info("Unzipping '%s' to '%s'" % (file_to_unzip, target_dir))
    if not os.path.isdir(target_dir):
        os.makedirs(target_dir)
        log.debug("Created directory %s to store unzipped files!" % target_dir)
    fh = open(file_to_unzip, 'rb')
    z = zipfile.ZipFile(fh)
    for i, filename in enumerate(z.namelist()):
        z.extract(filename, target_dir)
    fh.close()
    log.debug("Unzipped %s files from file %s." % (i + 1, file_to_unzip))


def untar_file(file_to_untar, target_dir):
    """
    Untar files in file_to_untar to target dir
    """
    log.info("Untaring '%s' to '%s'" % (file_to_untar, target_dir))
    if not os.path.isdir(target_dir):
        os.makedirs(target_dir)
        log.debug("Created directory %s to store untared files!" % target_dir)
    fh = tarfile.open(file_to_untar, 'r')
    fh.extractall(target_dir)
    fh.close()
    log.debug("Untared all files from file %s." % file_to_untar)


def extract_file(file_to_extract, target_dir):
    """
    Checks, whether the uploaded file is a zip  or tar file, or not.
    If it is a zip/tar file, it is unzipped/untared into a directory
    that has the same name as the query.
    """
    if zipfile.is_zipfile(file_to_extract):
        unzip_file(file_to_extract, target_dir)
    elif tarfile.is_tarfile(file_to_extract):
        untar_file(file_to_extract, target_dir)


def unzip_query(query):
    """
    If the user had zipped/tared a directory containing fast5 files,
    the directory structure after unzipping/untaring will likely look
    like this:
    extracted_files/
     name_of_the_zipped_dir/
      file1.fast5
      file2.fast5
        ...
    therefore, we need to move/link the directory below the dir
    'extracted_files' into the current working directory
    and rename it to the name of the query file
    """
    target_dir_name = 'extracted_files'
    # extract the archive
    extract_file(query, target_dir_name)
    subdir_with_seq_files = get_subdir_with_seq_files(target_dir_name)
    if subdir_with_seq_files:
        if zipfile.is_zipfile(query):
            shutil.move(query, query + '.zip')
        elif tarfile.is_tarfile(query):
            shutil.move(query, query + '.tar.gz')
        shutil.move(subdir_with_seq_files, query)
    else:
        log.error("No sequence files were found in the archive file provided!")
        sys.exit(1)


def detect_fastq_or_fast5_format(query):
    """
    Decides, whether the user supplied file is in fastq or fast5 format.
    If query is a folder, it checks the format of the first file in
    the first folder with sequence files.
    """
    # if unzipped folder, get dir containing sequence files
    # (might be this dir, or a subdir of it)
    if os.path.isdir(query):
        dirname = get_subdir_with_seq_files(query)
        for filename in os.listdir(dirname):
            if not filename.startswith('.'):
                query = os.path.join(dirname, filename)
                break
    fh = open(query, 'r')
    firstline = fh.readline()
    if firstline.startswith('@'):
        return 'fastq'
    elif firstline == '\x89HDF\r\n':
        return 'fast5'
    else:
        log.error("Could not detect format of query file. First line of query" +
                  " file was:\n'%s'" % firstline)
        sys.exit(1)


def concatenate_fastq_files(query):
    """
    If the user provided a zipped folder with several fastq files,
    concatenate all of these into one file.
    """
    input_filetype_query = detect_fastq_or_fast5_format(query)
    log.debug("Input filetype is %s." % input_filetype_query)
    if input_filetype_query == 'fastq':
        cat_outputfile = query + '.cat'
        concatenate_files(os.listdir(query), cat_outputfile, query)
        shutil.move(query, query + '.single_fastq_files')
        shutil.move(cat_outputfile, query)
    return query


def concatenate_files(filenames, output_filename, src_dir='.'):
    with open(output_filename, 'w') as outfile:
        for filename in filenames:
            with open(os.path.join(src_dir, filename)) as infile:
                outfile.write(infile.read())


def prepare_input_file(query, use_all_reads):
    """
    If the input file is an archive, it is extracted into a folder.
    If it is a single fastq file, this is directly used as input,
    if it is a fast5 file, this is converted into fastq.
    If it is a folder (also one that was extracted from an archive)
    containing several fast5 files, these will be converted into
    fastq files.
    """
    if os.path.isfile(query):
        if zipfile.is_zipfile(query) or tarfile.is_tarfile(query):
            log.debug("Input file is in zip/tar format")
            unzip_query(query)
        elif detect_fastq_or_fast5_format(query) == 'fastq':
            log.debug("Input file is in fastq format")
            return query
        elif detect_fastq_or_fast5_format(query) == 'fast5':
            log.debug("Input file is in fast5 format")
            return convert_fast5_to_fastq(query, use_all_reads)
        else:
            log.critical('Input file is in unknown file format!')
            sys.exit(1)
    if os.path.isdir(query):
        if detect_fastq_or_fast5_format(query) == 'fast5':
            return convert_fast5_to_fastq(query, use_all_reads)
        elif detect_fastq_or_fast5_format(query) == 'fastq':
            return concatenate_fastq_files(query)
        else:
            log.critical('Input file is in unknown file format!')
            sys.exit(1)


def which(program):
    """
    Checks, whether a programme exists in this environment
    and returns the path to its executable.
    Based on the answer to this StackOverflow question:
    http://stackoverflow.com/questions/377017/test-if-executable-exists-in-python
    """
    import os

    def is_exe(filepath):
        return os.path.isfile(filepath) and os.access(filepath, os.X_OK)

    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            path = path.strip('"')
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file
    log.critical("%s is not installed!" % (program))
    sys.exit(1)


def write_task_file(task):
    if not 'task' in os.listdir('.'):
        with open('task', 'w') as fh:
            fh.write(task)


def copy_file_or_dir(src_file, target_file):
    if os.path.isfile(src_file):
        shutil.copy(src_file, target_file)
    elif os.path.isdir(src_file):
        shutil.copytree(src_file, target_file)


def get_snp_info_dict(snp_info_file):
    """
    Returns a dictionary of the data in the snp info file
    that contains information on a specific polymorphism.

    The dict snp_info contains the chromosome name, chromosome
    position and pubmed ID as a key (in form of a tuple). The
    value is a list with the data from the snp_info_file.
    This data is given as a dictionary

    :param snp_info_file:
    :return: snp_info, a dictionary with nested dictionary
    """
    from collections import defaultdict
    snp_info = defaultdict(list)
    with open(snp_info_file, 'r') as fh:
        snp_dictreader = csv.DictReader(fh, dialect='excel-tab')
        for each_row in snp_dictreader:
            chrom = each_row['Chromosome']
            if each_row['Position'] == '-':
                continue
            chrom_pos = int(each_row['Position'])
            snp_info[(chrom, chrom_pos)].append(each_row)
    return snp_info


def combine_polymorphisms_file_and_snp_info(snp_info_file):
    """
    Writes 'known SNPs' to the file polymorphisms_snp_info.tsv
    'Known SNPs' are those, that have identical chromosome
    position as a SNP given in the known SNPs file
    """
    snp_info_total = get_snp_info_dict(snp_info_file)
    output_lines = []
    with open('polymorphisms.tsv', 'r') as fh:
        for line_nr, line in enumerate(fh.readlines()):
            # skip first line since it only contains the header
            if line_nr == 0:
                continue
            ls = line.strip().split('\t')
            chrom, chrom_pos = ls[11], int(ls[12])
            if (chrom, chrom_pos) in snp_info_total:
                snp_info_list = snp_info_total[(chrom, chrom_pos)]
            else:
                # move to next line, if this chrom position is not found in SNP info file
                continue
            for snp_info in snp_info_list:
                # iterate over each element of the snp_info_list
                this_line = ls[:]
                for snp_info_field in snp_info_fields:
                    # iterate over each column, in specific order for output; if we would iterate over dictionary
                    # keys, they would be in alphabetic order
                    if snp_info_field == "Publication":
                        link = '<a target="_blank" href="http://www.ncbi.nlm.nih.gov/pubmed/{0}">{0}</a>'.format(snp_info["Publication"])
                        this_line.append(link)
                    elif snp_info_field == "DrugResistanceMutation":
                        this_line.append(snp_info["DrugResistanceMutation"] + '\n')
                    else:
                        this_line.append(snp_info[snp_info_field])
                output_lines.append('\t'.join(this_line))
    output_file = open('polymorphisms_snp_info.tsv', 'w')
    output_file.write('\t'.join(snp_output_header + snp_info_fields) + '\n')
    output_file.writelines(output_lines)
    output_file.close()


class SingleLevelFilter(logging.Filter):
    """
    From http://stackoverflow.com/a/1383365/897521
    Needed for the creation of the log file
    """
    def __init__(self, passlevel, reject):
        self.passlevel = passlevel
        self.reject = reject

    def filter(self, record):
        if self.reject:
            return record.levelno != self.passlevel
        else:
            return record.levelno == self.passlevel


def main():
    """
    Main code
    """

    """
    Check, if lastal and last-split are installed
    """
    lastal_path = which('lastal')
    lastsplit_path = which('last-split')

    """
    Get input arguments
    """
    query, lastdb_path, input_task, target_file, use_all_reads, user_last_parameters, substitution_matrix\
        = get_input_arguments()
    write_task_file(input_task)
    if query != 'query':
        copy_file_or_dir(query, 'query')
        query = 'query'

    log.info('Started script nanopipe_calc.py (version %s) with following arguments: %s' % (__version__,
                                                                                            sys.argv))
    log.debug('Running in debug mode!')

    """
    If necessary, prepare the query file, i.e. if it is an archive,
    or a dir, or a fast5 file, to get a fastq file
    """
    query = prepare_input_file(query, use_all_reads)

    """
    Perform last alignment software on fastq files
    """
    maf_filename = 'last_output.maf'
    perform_last_alignment(query=query,
                           lastdb_path=lastdb_path,
                           input_task=input_task,
                           output_filename=maf_filename,
                           lastal_path=lastal_path,
                           lastsplit_path=lastsplit_path,
                           user_last_parameters=user_last_parameters,
                           substitution_matrix=substitution_matrix)

    """
    Check, if last produced any alignment
    """
    if not check_if_last_produced_alignment(maf_filename):
        log.error("The alignment programme last couldn't align the target and query.")
        sys.exit(1)

    """
    Convert maf files to tabular format
    """
    # remove lines starting with 'p' from maf file
    # these lines are generated by last, but are not understood by the maf parser
    remove_p_lines_command = ['grep', '-v', '^p', maf_filename]
    maf_filename_no_p_lines = maf_filename + '.edited'
    execute_command(remove_p_lines_command, maf_filename_no_p_lines, True)
    tabular_filename = 'last_output.tab'
    log.debug('maf2tabular_script is: %s' % maf2tabular_script)
    maf2tabular_command = ['python', maf2tabular_script, maf_filename_no_p_lines, tabular_filename]
    execute_command(command=maf2tabular_command, output_filename=tabular_filename, use_os_system=True)
    os.remove(maf_filename_no_p_lines)

    """
    Filter alignments with highest bitscores
    For each read only the alignment with the highest bitscore is kept
    """
    tabular_bestbitscore_filename = 'last_output_bestbitscore.tab'
    cmd = 'sort -k1,1 -k14,14nr %s | sort -u -k1,1 > %s' % (
        tabular_filename, tabular_bestbitscore_filename)
    os.system(cmd)

    """
    Count the nucleotides
    """
    nt_count_filename = 'nt_counts.tsv'
    nt_count_command = ['python', nt_count_script, tabular_bestbitscore_filename, nt_count_filename]
    execute_command(nt_count_command, nt_count_filename)

    """
    Plot the nucleotide counts
    """
    plot_command = ['Rscript', plot_nt_count_rscript, nt_count_filename]
    execute_command(plot_command, 'plot.pdf')

    """
    Get the number of reads per gene
    """
    reads_per_gene_filename = 'reads_per_gene.tsv'
    os.system('echo -e "Reads\tGene" > %s' % reads_per_gene_filename)
    cmd = '''cut -f 3 %s | sort | uniq -c | awk 'BEGIN{OFS="\t"}{print $1,$2}' >> %s''' % (
        tabular_bestbitscore_filename, reads_per_gene_filename)
    os.system(cmd)

    """
    Get the lengths of the reads, binned by the gene, then plot them
    """
    read_target_len_filename = 'len_per_target.tsv'
    cmd = 'cut -f 2,3 %s > %s' % (tabular_bestbitscore_filename, read_target_len_filename)
    os.system(cmd)
    plotlen_command = ['Rscript', plot_len_script, read_target_len_filename, "FALSE"]
    execute_command(plotlen_command, use_os_system=True)

    read_len_filename = 'len_per_read.tsv'
    cmd = r"""awk '{if(NR % 4==1){printf $0"\t"}else if(NR % 4==2){print length($0)}}' query | awk 'BEGIN{FS="\t"; OFS="\t"}{print $2, $1}'""".split()
    execute_command(cmd, output_filename=read_len_filename, save_stdout_to_file=True, use_os_system=True)
    plotlen_command = ['Rscript', plot_len_script, read_len_filename, "all_in_one"]
    execute_command(plotlen_command)

    """
    Create alignments of the reference and the consensus sequences
    """
    target_dict = {}
    for h, s in yield_fasta(target_file): target_dict[h] = s
    aln_nr = 0
    for header_c, sequence_c in yield_fasta('consensus.fasta'):
        header_c2 = header_c.replace('_consensus', '')
        qseq = ">%s\n%s" % (header_c, sequence_c)
        try:
            tseq = ">%s\n%s" % (header_c2, target_dict[header_c2])
        except KeyError:
            log.error("The target sequence file %s does not contain the sequence %s." % (target_file,
                                                                                         header_c2))
            sys.exit(1)
        muscle_input_filename = 'seq.%02d.fasta' % aln_nr
        muscle_output_filename = muscle_input_filename + '.html'
        with open(muscle_input_filename, 'w') as fh:
            fh.write('%s\n%s' % (qseq, tseq))
        perform_muscle_alignment(muscle_input_filename, muscle_output_filename)
        aln_nr += 1

    """
    Combine info on known SNPs and found polymorphisms
    """
    if input_task == 'plasmodium':
        combine_polymorphisms_file_and_snp_info(snp_info_file)

    """
    Generate results.hbi
    """
    execute_command(['python', generate_results_script])
    log.info("Script finished.")

    """
    Delete query files from server
    """
    for filename in os.listdir('.'):
        # if filename not in files2keep and not filename.upper() in files2keep_matches:
        if filename in ['extracted_files', 'query.fast5', 'query.zip', 'query.tar.gz']:
            if os.path.isfile(filename):
                os.remove(filename)
            elif os.path.isdir(filename):
                shutil.rmtree(filename)


"""
set up the  log file and standard output log info
"""
log = logging.getLogger('log')
log_fh = logging.FileHandler(get_time().replace(" ", "_") + '.log')
ch = logging.StreamHandler()
formatter = logging.Formatter('[%(asctime)s] %(levelname)9s: %(message)s', datefmt='%Y-%m-%d %I:%M:%S')
log_fh.setFormatter(formatter)
ch.setFormatter(formatter)
log.addHandler(ch)
log.addHandler(log_fh)
log.setLevel(logging.WARNING)
log_fh.setLevel(logging.INFO)

if __name__ == "__main__":
    main()
