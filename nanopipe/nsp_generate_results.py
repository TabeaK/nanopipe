#!/usr/bin/env python

"""
Creates png/pdf and hbi files.
"""
import os
import sys
from nanopipe_calc import get_time

table_class_default = ' class="default"'
table_class_sortable = ' class="staticheader default sortable" '
table_class_sortable_height = table_class_sortable + ' height="625px" '
header_polymorphisms = ['Sequence name',
                        'Position in gene',
                        'Reference',
                        'Consensus',
                        'A',
                        'C',
                        'G',
                        'T',
                        'gap',
                        'Total read nr',
                        'Variation',
                        'Chromosome',
                        'Position on chromosome']
iupac_table = """
                <p>
                    Sequences are given in IUPAC notation:
                    <table class="default">
                        <thead>
                            <tr>
                                <th>IUPAC code</th>
                                <th>Meaning</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                            	<td>R</td>
                            	<td>A or G</td>
                            </tr>
                            <tr>
                            	<td>Y</td>
                            	<td>C or T</td>
                            </tr>
                            <tr>
                            	<td>K</td>
                            	<td>G or T</td>
                            </tr>
                            <tr>
                            	<td>M</td>
                            	<td>A or C</td>
                            </tr>
                            <tr>
                            	<td>S</td>
                            	<td>C or G</td>
                            </tr>
                            <tr>
                            	<td>W</td>
                            	<td>A or T</td>
                            </tr>
                            <tr>
                            	<td>B</td>
                            	<td>C, G or T</td>
                            </tr>
                            <tr>
                            	<td>D</td>
                            	<td>A, G or T</td>
                            </tr>
                            <tr>
                            	<td>H</td>
                            	<td>A, C or T</td>
                            </tr>
                            <tr>
                            	<td>V</td>
                            	<td>A, C or G</td>
                            </tr>
                            <tr>
                            	<td>N</td>
                            	<td>A, C, G or T</td>
                            </tr>
                        </tbody>
                    </table>
                 </p>"""


def get_html_th(item):
    return '\t\t<th>%s</th>\n' % item


def get_html_td(item):
    return '\t\t<td>%s</td>\n' % item


def get_html_tr(item):
    return '\t<tr>\n%s\t</tr>\n' % item


def get_html_table(item, table_class=''):
    # return '<table %s width="100%%">\n%s</table>'%(table_class, item)
    return '<table %s __width="100%%">\n%s</table>' % (table_class, item)


def get_html_th_row(items):
    """
    items needs to be a list.
    """
    html_cols = ''
    for elem in items:
        html_cols += get_html_th(elem)
    return get_html_tr(html_cols)


def get_html_td_row(items):
    """
    items needs to be a list.
    """
    html_cols = ''
    for elem in items:
        html_cols += get_html_td(elem)
    return get_html_tr(html_cols)


def tsv_to_html_table(filename,
                      has_header=True,
                      header_text=None,
                      table_class=table_class_default):
    """
    Takes a tab-separated input file and creates a html table from it.
    """
    fh = open(filename, 'r')
    lines = fh.readlines()
    fh.close()
    html_rows = ''
    for line_nr, line in enumerate(lines):
        linesplit = line.split('\t')
        if line_nr == 0:
            if has_header and not header_text:
                html_rows += '<thead>\n%s</thead>' % get_html_th_row(linesplit)
            elif (has_header and header_text) or (not has_header and header_text):
                html_rows += '<thead>\n%s</thead>' % get_html_th_row(header_text)
            elif not has_header and not header_text:
                print("Please specify the header to write in the table!")
                sys.exit(1)
        else:
            html_rows += get_html_td_row(linesplit)
    table = get_html_table(html_rows, table_class)
    return table


def fasta_to_html(filename):
    """
    Takes a fasta file and creates html code displaying the sequences nicely
    """
    from Bio import SeqIO
    from textwrap import wrap

    html = ''
    fh = open(filename, 'r')
    seqs = {}
    for record in SeqIO.parse(fh, 'fasta'):
        seqs[record.id] = record
    i = 0
    fh.close()
    sorted_record_ids = sorted(seqs.keys())
    for record_id in sorted_record_ids:
        record = seqs[record_id]
        seq = record.seq._data
        seq = '\n'.join(wrap(seq, 100))
        data = {'id': record.id, 'no': i, 'seq': seq}
        html += """\n\t<div addons="toggleheader('{id}')">
                    <pre>\n&gt;{id}\n{seq}\n\t\t</pre>\n\t</div><br>""".format(**data)
        i += 1
    return html


def split_string(s, n=50):
    """Splits a string into chunks of size n
    and returns a list of these chunks.
    """
    return [s[i:i + n] for i in range(0, len(s), n)]


def add_colors_to_aln_seq(aln_seq, identity_seq):
    """Returns a modified alignment sequence, where each alignment position
    that is a mismatch is colored orange.
    """
    colored_seq = list(aln_seq)
    mismatch_index = [i for i, ltr in enumerate(identity_seq) if ltr == ":"]
    for mismatch_pos in mismatch_index[::-1]:
        colored_seq[mismatch_pos] = '<div class="marked">%s</div>' % colored_seq[mismatch_pos]
    return ''.join(colored_seq)


def adjust_sequence_name(name, maxlen=30):
    """
    Will shorten a sequence name that is longer
    than maxlen characters by 3 characters and
    adds dots to indicate it was shortened.
    """
    if len(name) >= maxlen:
        return name[0:maxlen-3] + '...'
    else:
        return name


def fasta_aln_to_html():
    from Bio import AlignIO

    html = ''
    for filename in sorted(os.listdir('.')):
        if not filename.endswith('.html'):
            continue

        aln_html = ''

        align = AlignIO.read(filename, 'fasta')

        seq1 = str(align[0].seq)
        seq2 = str(align[1].seq)
        iden = ""

        # adjust names, if they are too long
        name1_orig = "Consensus"
        name2_orig = align[1].id.split(':')[-1]
        name_maxlen = max(len(name1_orig), len(name2_orig))
        if name_maxlen > 30:
            name_maxlen = 30
        name1 = adjust_sequence_name(name1_orig, maxlen=name_maxlen)
        name2 = adjust_sequence_name(name2_orig, maxlen=name_maxlen)
        name1 = name1.rjust(name_maxlen)
        name2 = name2.rjust(name_maxlen)

        # if the entire consensus sequence is N the consensus couldn't be called,
        # thus no alignment will be generated!
        if set(seq1) == set("N"):
            html += """
                <style type="text/css">
                    #www_data .marked {{
                        color: #ffffff;
                        background-color: #eb6841;
                        display: inline-block;
                        font-family:Bitstream Vera Sans Mono, Courier;
                    }}
                </style>
                <div addons="toggleheader('Alignment for {name2}')">
                <div style="font-family:Bitstream Vera Sans Mono, Courier; white-space: pre;">{alignment}
                </div><br></div></br>\n""".format(**{'name2': name2_orig,
                                                     'alignment': "The consensus couldn't be called, "
                                                                  "probably because there were not enough nanopore"
                                                                  " reads aligning to this target sequence."})
            continue

        # get 'identity' string, which is printed between sequences
        for i, nt1 in enumerate(seq1):
            nt2 = seq2[i]
            if nt1 == nt2:
                iden += "|"
            elif nt1 == "-" or nt2 == "-":
                iden += " "
            elif nt1 != nt2:
                iden += ":"
        namei = " " * name_maxlen

        # split the sequences into chunks
        seq1w = split_string(seq1)
        seq2w = split_string(seq2)
        idenw = split_string(iden)
        # get counters
        seq1cnt = 0
        seq2cnt = 0
        # generate the alignment display
        aln_html += "Gaps: %s (%s %%)\n" % (iden.count(' '), round(iden.count(' ') / float(len(iden)) * 100, 2))
        aln_html += "Identity: %s %%\n" % (round(iden.count('|') / float(len(iden)) * 100, 2))
        end_of_n, start_of_n = seq1.find(seq1.strip('N')), seq1.find(seq1.strip('N')) + len(seq1.strip('N'))
        aln_html += "Identity (leading and trailing 'N' ignored): %s %%<br/>\n" % \
                    (round(iden[end_of_n:start_of_n].count('|') / float(len(iden[end_of_n:start_of_n])) * 100, 2))
        aln_html += "Number of 'N': %s (%s %%)\n" % (
            seq1.count('N'), round(seq1.count('N') / float(len(seq1)) * 100, 2))

        for row_nr in range(0, len(seq1w)):
            seq1cnt_p = len(seq1w[row_nr]) - seq1w[row_nr].count('-')
            seq2cnt_p = len(seq2w[row_nr]) - seq2w[row_nr].count('-')
            aln_html += "<br/>%s %04d %s %04d\n" % (name1, seq1cnt + 1,
                                                    add_colors_to_aln_seq(seq1w[row_nr], idenw[row_nr]),
                                                    seq1cnt + seq1cnt_p)
            aln_html += "%s      %s\n" % (namei, idenw[row_nr])
            aln_html += "%s %04d %s %04d\n" % (name2, seq2cnt + 1,
                                               add_colors_to_aln_seq(seq2w[row_nr], idenw[row_nr]),
                                               seq2cnt + seq2cnt_p)
            seq1cnt += seq1cnt_p
            seq2cnt += seq2cnt_p

        html += """
            <style type="text/css">
                #www_data .marked {{
                    color: #ffffff;
                    background-color: #eb6841;
                    display: inline-block;
                    font-family:Bitstream Vera Sans Mono, Courier;
                }}
            </style>
            <div addons="toggleheader('Alignment for {name2}')">
            <div style="font-family:Bitstream Vera Sans Mono, Courier; white-space: pre;">{alignment}
            </div><br></div></br>\n""".format(**{'name2': name2_orig,
                                                 'alignment': aln_html})
    return html


def get_task():
    """
    Returns the task chosen by the user by reading the content of
    the file ''
    """
    with open('task') as fh:
        task = fh.read()
    return task.strip()


def get_job_id():
    """
    Returns the job id, based on the current working directory.
    """
    return os.path.split(os.getcwd())[-1][1:]


def get_title(filename='.title'):
    """
    Reads the content of the file title. If no title was given,
    return the name of the task.
    """
    title_by_user = ''
    job_id = get_job_id()
    task_text = {'plasmodium': "Plasmodium Polymorphisms",
                 'dengue': "Dengue Virus Serotype Classification",
                 'provide': "Provide Target File"}
    if filename in os.listdir('.'):
        with open(filename, 'r') as fh:
            title_by_user = fh.read()
    if not title_by_user or title_by_user == '':
        title = " <br>Job ID: %s<br>Discovery Task: %s" % (job_id, task_text[get_task()])
    else:
        title = " for: '%s' <br>Job ID: %s<br>Discovery Task: %s" % (title_by_user, job_id, task_text[get_task()])
    return title


def get_html_for_split_tables(filename,
                              splitcol,
                              has_header=True,
                              header_text=None,
                              table_class=table_class_default):
    """
    Takes the name of a tsv file containing a table and the column
    number that should be used to split the file.
    Returns a dictionary, in which the content of the column is the
    key and a string of html code is the value.
    For each unique content of the column splitcol, a new item in the
    dictionary is created.
    Thus, you get one table for each unique item in the splitcol.
    Returns a list!
    """
    from collections import defaultdict
    d = defaultdict(list)
    header = ''
    with open(filename, 'r') as fh:
        for i, line in enumerate(fh.readlines()):
            if has_header and i == 0:
                header = line
                continue
            linesplit = line.split()
            name = linesplit[splitcol]
            d[name].append(line)
    html_d = {}
    for name in d.keys():
        rows = d[name]
        fh = open('.table.temp', 'w')
        fh.write(header + ''.join(rows))
        fh.close()
        if len(rows) >= 20:
            adapted_table_class = table_class
        else:
            adapted_table_class = table_class.split("""height=""")[0]
        html_d[name] = tsv_to_html_table(filename='.table.temp',
                                         has_header=False,
                                         header_text=header_text,
                                         table_class=adapted_table_class)
    return html_d


def get_polymorphisms_table(filename='polymorphisms.tsv'):
    """
    create polymorphisms table
    """
    poly_dict = get_html_for_split_tables(filename=filename,
                                          splitcol=0,
                                          has_header=True,
                                          header_text=header_polymorphisms,
                                          table_class=table_class_sortable_height)
    html_poly = ''
    for i, gene_name in enumerate(sorted(poly_dict.keys())):
        html_table = poly_dict[gene_name]
        html_poly += """<div addons="toggleheader('%s')"></br>\n""" % gene_name
        html_poly += html_table + "</div></br>\n"
    if html_poly == '':
        html_poly = "No Polymorphisms were found. </div></br>\n"
    return html_poly


def get_lengths_plot(filename='lengths.png'):
    html = '<img src="%s" alt="Lengths of Nanopore reads" width="500px">' % \
           get_link(filename)
    return html


def plots_to_html(sbjct_names_list):
    html = '<ul>'
    i = 0
    for filename in sorted(os.listdir('.')):
        if not (filename.startswith('plot') and filename.endswith('.pdf') and 'polym' not in filename):
            continue
        pdf_link = get_link(filename.replace('.png', '.pdf'))
        try:
            sbjct_name = sbjct_names_list[i]
        except IndexError:
            sbjct_name = "nA"
        html += '<li><a href="%s">%s</a></li>' % (pdf_link, sbjct_name)
        i += 1
    return html


def get_link(filename):
    """
    Returns a link to one of the files produced during analysis.
    """
    jobid = os.path.split(os.getcwd())[1][1:]
    return '/tools/NanoPipe/data/x%s/%s' % (jobid, filename)


def get_sbjct_names_list(filename):
    sbjct_names_list = []
    with open(filename, 'r')as fh:
        for i, line in enumerate(fh.readlines()):
            if i == 0:
                continue
            sbjct_names_list.append(line.split()[1])
    return sbjct_names_list


def get_fastq_read_number(filename):
    """
    Returns the number of reads in a fastq file.
    """
    from Bio import SeqIO

    nr_reads = len(list(SeqIO.parse(filename, 'fastq')))
    if not isinstance(nr_reads, int):
        return False
    return nr_reads


def get_target_counts():
    """
    Returns the target gene's names and the counts of nanopore
    reads that aligned to it in two tuples.
    These tuples are sorted, with the highest number in the beginning.
    """
    import operator

    counts = []
    with open('reads_per_gene.tsv', 'r') as fh:
        for i, line in enumerate(fh.readlines()):
            if i == 0:
                continue
            count, name = line.split()
            counts.append((name, int(count)))
            print (name, int(count))
        counts.sort(key=operator.itemgetter(1))
    return counts[::-1]


def get_last_parameter_display():
    """
    Returns a display of the last parameters
    used, taken from the head of the initial last output file.

      1 # LAST version 572
      2 #
      3 # a=21 b=1 A=1 B=1 e=180 d=65 x=179 y=43 z=179 D=100000 E=1.51359e+08
      4 # R=10 u=0 s=2 T=0 m=10 l=1 n=10 k=1 i=8192 w=1000 t=4.3311 g=1 j=3 Q=1
      5 # /bioinf/projects/Nanopore/appl/calculate/data/myDB_plasmodium_falciparum
      6 # Reference sequences=13 normal letters=33034
      7 #
      8 #    A  C  G  T
      9 # A  4 -12 -12 -12
     10 # C -12  9 -12 -12
     11 # G -12 -12  9 -12
     12 # T -12 -12 -12  4

    """
    lines_to_keep  = [0, 2, 3, 7, 8, 9, 10, 11, 12]
    parameters = []
    with open("last_output.maf_before_split", "r") as fh:
        for i, line in enumerate(fh.readlines()):
            if i in lines_to_keep:
                parameters.append(line)
            if i == lines_to_keep[-1]:
                # stop after the last line to be kept
                break
    html = "<div class='section'>" \
           "<p>The following LAST parameters were used to produce " \
           "the alignments with the command lastal: </p>" \
           "<p><pre>%s</pre></p>"\
           "</div>" % ''.join(parameters)
    return html


def get_summary_txt():
    """
    Writes a summary of the results to the file summary.txt
    """
    task = get_task()
    title = get_title()
    txt = ("<h1>Results %s (%s)</h1>" % (title, task))
    txt += "<p><font size='6'>finished on %s</font></p>" % get_time()
    txt += "<p>Your query contained %s reads.</p>" % get_fastq_read_number('query')
    if task == 'dengue':
        target_counts = get_target_counts()
        txt += "<p>The most common Dengue Serotype in your data "
        txt += "is <b>%s</b>, with %s reads aligning to it.</p> " % (target_counts[0][0], target_counts[0][1])
        txt += "<p>The total number of Nanopore reads that aligned "
        txt += "to any of the four Dengue Serotypes is %s.</p>" % sum(x[1] for x in target_counts)
    txt += get_last_parameter_display()
    return txt


def main():
    results = {'alignments': fasta_aln_to_html(),
               'reads_per_gene': tsv_to_html_table('reads_per_gene.tsv', table_class=table_class_sortable),
               'consensus': fasta_to_html('consensus.fasta'),
               'consensus_file': get_link('consensus.fasta'),
               'known_SNPs_file': get_link('polymorphisms_snp_info.tsv'),
               'lengths_target_img_url': 'len_per_target.tsv_lengths_',
               'lengths_read_img_url': get_link('len_per_read.tsv_lengths.png'),
               'link_to_alignments_hbi': get_link('alignments.hbi'),
               'link_to_consensus_hbi': get_link('consensus.hbi'),
               'link_to_length_hist': get_link('lengths.hbi'),
               'link_to_plots_hbi': get_link('plots.hbi'),
               'link_to_polymorphisms_hbi': get_link('polymorphisms.hbi'),
               'link_to_read_nr_per_gene_hbi': get_link('read_nr_per_gene.hbi'),
               'link_to_result_hbi': get_link('result.hbi'),
               'plots': plots_to_html(get_sbjct_names_list('reads_per_gene.tsv')),
               'polymorphisms': get_polymorphisms_table(),
               'summary': get_summary_txt(),
               'title': get_title(),
               'iupac_table': iupac_table,
               'job_id': get_job_id()
    }
    if 'polymorphisms_snp_info.tsv' in os.listdir('.'):
        results['known_polymorphisms'] = tsv_to_html_table('polymorphisms_snp_info.tsv',
                                                        table_class="""class="default""""")
        results['link_to_known_polymorphisms_hbi'] = get_link('known_polymorphisms.hbi')

    html_menu = """
                <menu>
                 <ul>
                  <li><div align="center"><img src="/tools/NanoPipe/generate/images/logo_small.png"
                      alt="NSP Logo" width="150px"></div></li>
                  <li><a href="/tools/NanoPipe/index.hbi">About</a></li>
                  <li><a href="/tools/NanoPipe/usage">Usage</a></li>
                  <li><a href="/tools/NanoPipe/generate">Run the Pipeline</a></li>
                  <li><a href="/tools/NanoPipe/download-cline/about.hbi">Download Software</a></li>
                  <li><a href="/tools/NanoPipe/download-reads_index.hbi">Download Datasets</a></li>
                  <li><a href="/tools/NanoPipe/supp">Supplementary Material</a></li>
                  <li><a href="mailto:bioinfo@uni-muenster.de?subject=Nano Pipe">Contact</a></li>

                  <li><div class="header">Result {title}</div></li>
                  <li><a href="{link_to_result_hbi}">Overview</a></li>
                  <li><a href="{link_to_length_hist}">Read length distribution</a></li>
                  <li><a href="{link_to_read_nr_per_gene_hbi}">Number of reads per target</a></li>
                  <li><a href="{link_to_polymorphisms_hbi}">Polymorphisms</a></li>
                  """.format(**results)
    if 'polymorphisms_snp_info.tsv' in os.listdir('.'):
        html_menu += '<li><a href="{link_to_known_polymorphisms_hbi}">Known polymorphisms</a></li>'.format(**results)
    html_menu += """
                  <li><a href="{link_to_consensus_hbi}">Consensus sequences</a></li>
                  <li><a href="{link_to_alignments_hbi}">Alignments</a></li>
                  <li><a href="{link_to_plots_hbi}">Logo plots</a></li>
                  <li><a href="/tools/NanoPipe/generate/storagetime/index.pl?id={job_id}">(Extend Duration)</a></li>
                 </ul>
                </menu>
                """.format(**results)
    from pprint import pprint
    print(pprint(results))
    html_body = html_menu + """
                {summary}
                """.format(**results)

    html_body_lengths = html_menu + """
                <h2>Read length distribution</h2>
                <h3>Read length distribution for aligned reads</h3>
                 <p>The following histograms show the lengths of reads that aligned
                 to a target sequence.</p>
                 <p><img src={lengths_target_img_url} alt="Read length distribution<" width="100%"></p>
                <h3>Read length distribution for all reads</h3>
                 <p>The following histograms show the lengths of all reads.</p>
                 <p><img src={lengths_read_img_url} alt="Read length distribution<" width="30%"></p>

                 """.format(**results)
    for fi in os.listdir('.'):
        if results["lengths_target_img_url"] in fi:
            html_body_lengths += """
                <p><img src={} alt="Read length distribution<" width="30%"></p>
                """.format(fi)

    html_body_consensus = html_menu + """
                <h2>Consensus sequences of aligned reads</h2>
                 <a name="consensus" style="display: none">consensus</a>
                 {iupac_table}
                 <p>You can download all consensus sequences in fasta format
                    <a href="{consensus_file}" download="consensus.fasta">here</a>.
                 </p>
                {consensus}
                """.format(**results)
    if 'polymorphisms_snp_info.tsv' in os.listdir('.'):
        html_body_known_polymorphisms = html_menu + """
                    <h2>Known Polymorphisms found in your data</h2>
                    <p>
                        The following table gives the polymorphisms found in your data that are
                        also covered in the literature. Only polymorphisms that were covered by
                        at least ten Nanopore reads in the alignments are considered.
                    </p>
                    <p>
                        The information on known SNPs is taken from the following resources:
                        <ul>
                         <li><a href="https://www.malariagen.net/apps/pf/3.1/" noarrow>MalariaGEN database</a>, see
                                <a href="http://www.ncbi.nlm.nih.gov/pubmed/22722859" noarrow>Manske, Miotto,
                                <em>et al.</em>
                                (2012)</a></li>
                         <li><a href="doi:10.1101/gr.168286.113" noarrow>Nair <em>et al.</em> (2014).</a></li>
                        </ul>
                    </p>

                 <h3>Explanation of the Columns</h3>
                 <ul>
                  <li><i>Sequence name:</i> The name of the target sequence/the gene</li>
                  <li><i>Position:</i>  The position in the target sequence (this is not based on the entire
                                        gene sequence)</li>
                  <li><i>Reference:</i> The nucleotide in the target sequence at this position</li>
                  <li><i>Consensus:</i> The most common (majority rule) nucleotide in the Nanopore reads that
                                        align to this position in the target sequence. This
                                        might be given in IUPAC notation. The IUPAC notation is explained below</li>
                  <li><i>A:</i> How often the nucleotide "A" is found in the Nanopore read at
                                        this position of the target sequence.</li>
                  <li><i>C:</i> How often the nucleotide "C" is found in the Nanopore read at
                                        this position of the target sequence.</li>
                  <li><i>G:</i> How often the nucleotide "G" is found in the Nanopore read at
                                        this position of the target sequence.</li>
                  <li><i>T:</i> How often the nucleotide "T" is found in the Nanopore read at
                                        this position of the target sequence.</li>
                  <li><i>gap:</i> How often a gap is found in the Nanopore read at this
                                        this position of the target sequence.</li>
                  <li><i>Total reads:</i> The total number of Nanopore read/Target alignments that do not
                                        have a gap at this position.</li>
                  <li><i>Variation:</i> The variation in the nucleotides found in the Nanopore reads at
                                        this position of the target sequence. It is calculated by dividing
                                        the number of occurrences of the most common nucleotide by the total
                                        number of nucleotides.</li>
                  <li><i>Chromosome:</i> The chromosome on which the gene is located.</li>
                  <li><i>Position on chromosome:</i> The position on the chromosome.</li>
                  <li><i>AminoAcid:</i> The amino acid change.</li>
                  <li><i>NRAF_WAF:</i> The non-reference allele frequency (NRAF)
                         for west african population  (WAF).</li>
                  <li><i>NRAF_CAF:</i> The non-reference allele frequency (NRAF)
                         for central african population  (CAF).</li>
                  <li><i>NRAF_EAF:</i> The non-reference allele frequency (NRAF)
                         for east african population  (EAF).</li>
                  <li><i>NRAF_SAS:</i> The non-reference allele frequency (NRAF)
                         for south asian population  (SAS).</li>
                  <li><i>NRAF_WSEA:</i> The non-reference allele frequency (NRAF)
                         for western south-east asian population (WSEA).</li>
                  <li><i>NRAF_ESEA:</i> The non-reference allele frequency (NRAF)
                         for  eastern south-east asian population (ESEA).</li>
                  <li><i>NRAF_PNG:</i> The non-reference allele frequency (NRAF)
                                        for papua new guinean population (PNG).</li>
                  <li><i>NRAF_SAM:</i> The non-reference allele frequency (NRAF)
                         for south american population  (SAM).</li>
                  <li><i>MAF_WAF:</i> The minor allele frequency (MAF)
                         for west african population  (WAF).</li>
                  <li><i>MAF_CAF:</i> The minor allele frequency (MAF)
                         for central african population  (CAF).</li>
                  <li><i>MAF_EAF:</i> The minor allele frequency (MAF)
                         for east african population  (EAF).</li>
                  <li><i>MAF_SAS:</i> The minor allele frequency (MAF)
                         for  south asian population  (SAS).</li>
                  <li><i>MAF_WSEA:</i> The minor allele frequency (MAF)
                         for  western south-east asian population (WSEA).</li>
                  <li><i>MAF_ESEA:</i> The minor allele frequency (MAF)
                         for eastern south-east asian population (ESEA).</li>
                  <li><i>MAF_PNG:</i> The minor allele frequency (MAF)
                                        for papua new guinean population (PNG).</li>
                  <li><i>MAF_SAM:</i> The minor allele frequency (MAF)
                         for south american population  (SAM).</li>
                  <li><i>RefAllele:</i> The reference allele.</li>
                  <li><i>NonrefAllele:</i> The non-reference allele.</li>
                  <li><i>PrivatePopulation:</i> The population to which this SNP is private,
                    meaning that it is only found there.</li>
                  <li><i>Mutation:</i> Whether the mutation is synonymous (SYN) or nonsynonymous (NON).</li>
                  <li><i>DrugResistanceMutation:</i>
                    Information on drug resistance mutations from
                    <a href="doi:10.1101/gr.168286.113">Nair <em>et al.</em> (2014)</a>.
                    <ul>
                        <li>SP = Sulphadoxine-pyrimethamine</li>
                        <li>CQ = chloroquine</li>
                        <li>MQ = mefloquine</li>
                        <li>ART = artemisinin</li>
                    </ul>
                  </li>
                  <li><i>Publication:</i> The Pubmed ID of the publication from which
                  information on the SNP was taken from.</li>
                  {iupac_table}
                 </ul>


                    <div style="overflow:auto">
                    {known_polymorphisms}
                    </div>
                    """.format(**results)

    html_body_polymorphisms = html_menu + """
                <h2>Polymorphisms found in your data</h2>
                The following tables give the polymorphisms found in your data.
                <h3>How the Polymorphisms are Detected:</h3>
                <p>
                For each Nanopore read only the highest scoring pairwise alignment to a target sequence
                is chosen, the others are discarded.
                These alignments are then analysed to create a consensus sequence and to get the polymorphisms
                in the Nanopore reads.</p>
                <p>To get the Polymorphisms, the nucleotides in the Nanopore reads at each position
                of the target sequences are counted. Then, for each position in the target sequence
                the most common nucleotide in all Nanopore reads is chosen as the consensus nucleotide.
                Whenever the consensus nucleotide is not the same as the one in the reference sequence
                at this position, it is declared as polymorphic. Only polymorphisms that were covered by
                at least ten Nanopore reads in the alignments are considered.</p>

                <h3>Explanation of the Columns</h3>
                 <ul>
                  <li><i>Sequence name:</i> The name of the target sequence/the gene</li>
                  <li><i>Position:</i>  The position in the target sequence (this is not based on the
                                        entire gene sequence)</li>
                  <li><i>Reference:</i> The nucleotide in the target sequence at this position</li>
                  <li><i>Consensus:</i> The most common (majority rule) nucleotide in the Nanopore reads that
                                        align to this position in the target sequence. This
                                        might be given in IUPAC notation. The IUPAC notation is explained below</li>
                  <li><i>A:</i> How often the nucleotide "A" is found in the Nanopore read at
                                        this position of the target sequence.</li>
                  <li><i>C:</i> How often the nucleotide "C" is found in the Nanopore read at
                                        this position of the target sequence.</li>
                  <li><i>G:</i> How often the nucleotide "G" is found in the Nanopore read at
                                        this position of the target sequence.</li>
                  <li><i>T:</i> How often the nucleotide "T" is found in the Nanopore read at
                                        this position of the target sequence.</li>
                  <li><i>gap:</i> How often a gap is found in the Nanopore read at this
                                        this position of the target sequence.</li>
                  <li><i>Total reads:</i> The total number of Nanopore read/Target alignments that do not
                                        have a gap at this position.</li>
                  <li><i>Variation:</i> The variation in the nucleotides found in the Nanopore reads at
                                        this position of the target sequence. It is calculated by dividing
                                        the number of occurrences of the most common nucleotide by the total
                                        number of nucleotides.</li>
                 <li><i>Chromosome:</i> The chromosome on which the gene is located.</li>
                  <li><i>Position on chromosome:</i> The position on the chromosome.</li>
                  </ul>
                  {iupac_table}
                 <a name="polymorphisms" style="display: none">polymorphisms</a>
                {polymorphisms}
                """.format(**results)

    html_body_read_nr_per_gene = html_menu + """
                <h2>Number of reads per target</h2>
                 <p>The following table shows the number of reads that aligned
                 to a target sequence. Each read is counted only once. Thus,
                 if one read aligned to more than one target sequence, only
                 its highest scoring alignment (based on the bit score in the
                 LAST output file) is used.</p>
                 <a name="reads_per_gene" style="display: none">reads_per_gene</a>
                {reads_per_gene}
                """.format(**results)

    html_body_alignments = html_menu + """
                <h2>Alignments of consensus sequences to targets</h2></br>
                    <p>The entire target sequence is displayed and mismatches between the target
                    and the consensus sequence are indicated in orange.</p>
                    <p>The consensus sequence is generated from the Nanopore reads given as query file.</p>
                                      {iupac_table}
                    {alignments}
                """.format(**results)

    html_body_plots = html_menu + """
                <h2>Plots of Nanopore reads</h2>
                {plots}
                """.format(**results)

    fh = open('result.hbi', 'w')
    fh.write(html_body)
    fh.close()

    fh = open('consensus.hbi', 'w')
    fh.write(html_body_consensus)
    fh.close()

    fh = open('polymorphisms.hbi', 'w')
    fh.write(html_body_polymorphisms)
    fh.close()

    if 'polymorphisms_snp_info.tsv' in os.listdir('.'):
        fh = open('known_polymorphisms.hbi', 'w')
        fh.write(html_body_known_polymorphisms)
        fh.close()

    fh = open('lengths.hbi', 'w')
    fh.write(html_body_lengths)
    fh.close()

    fh = open('read_nr_per_gene.hbi', 'w')
    fh.write(html_body_read_nr_per_gene)
    fh.close()

    fh = open('alignments.hbi', 'w')
    fh.write(html_body_alignments)
    fh.close()

    fh = open('plots.hbi', 'w')
    fh.write(html_body_plots)
    fh.close()


if __name__ == "__main__":
    main()
